const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  // transpileDependencies: true,
  configureWebpack: {
    performance: {
        hints: "warning", // enum
        maxAssetSize: 1048576, // int (in bytes),
        maxEntrypointSize: 1048576, // int (in bytes)
    }
},
publicPath: "./"
})
