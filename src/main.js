import Vuelidate from '@vuelidate/core'

import { library } from '@fortawesome/fontawesome-svg-core'


import { faXmark } from '@fortawesome/free-solid-svg-icons'
import {faMagnifyingGlass} from '@fortawesome/free-solid-svg-icons'

import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faAngleDown } from "@fortawesome/free-solid-svg-icons";

library.add(faCoffee)
library.add(faAngleDown)
library.add(faXmark)
library.add(faMagnifyingGlass)



import { createApp } from 'vue'
import App from './App.vue'

const app = createApp(App)

app.component('font-awesome-icon', FontAwesomeIcon)

app.use(Vuelidate)
app.mount('#app')
